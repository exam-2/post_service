package main

import (
	"net"
	"temp/config"
	pp "temp/genproto/post"
	"temp/pkg/db"
	"temp/pkg/logger"
	"temp/pkg/messagebroker"
	"temp/services"
	"temp/services/client"
	"temp/kafka"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "post-service")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",	
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase)) 

	connDB, err := db.ConnectToDB(cfg)	
	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}
	ReviewClient, err := client.New(cfg)
	if err != nil{
		log.Fatal("Sqlx connection to postgres ERROR")
	}
	//Kafka
	produceMap := make(map[string]messagebroker.Producer)
	postTopicProduce := kafka.NewKafkaProducer(cfg, log, "post.post")
	defer func() {
		err := postTopicProduce.Stop()
		if err != nil {
			log.Fatal(`Failed to stopping Kafka`, logger.Error(err))
		}
	}()
	produceMap["post"] = postTopicProduce

	productService := services.NewPostService(connDB, log, ReviewClient, produceMap)

	lis, err := net.Listen("tcp",":" + cfg.PostServicePort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
	
	s := grpc.NewServer()
	reflection.Register(s)
	pp.RegisterPostServiceServer(s, productService)
	log.Info("main: server running",
		logger.String("port",cfg.PostServicePort))

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
