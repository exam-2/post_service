FROM golang:1.19.3-alpine
RUN mkdir post_service
COPY . /post_service
WORKDIR /post_service
RUN go mod tidy
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 2222