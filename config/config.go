package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment      string // develop, staging, production
	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	LogLevel         string
	RPCPort          string
	
	CustomServiceHost string
	CustomServicePort string

	PostServiceHost string
	PostServicePort string

	ReviewServiceHost string
	ReviewServicePort string

	KafkaHost string
	KafkaPort int
}

// Load loads environment variables and inflates Config

func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))

	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "posts"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "citizenfour"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "12321"))
	
	c.CustomServiceHost = cast.ToString(getOrReturnDefault("CUSTOMER_SERVICE_HOST", "localhost"))
	c.CustomServicePort = cast.ToString(getOrReturnDefault("CUSTOMER_SERVICE_PORT", "3333"))

	c.PostServiceHost = cast.ToString(getOrReturnDefault("POST_SERVICE_HOST", "localhost"))
	c.PostServicePort = cast.ToString(getOrReturnDefault("POST_SERVICE_PORT", "2222"))

	c.ReviewServiceHost = cast.ToString(getOrReturnDefault("REVIEW_SERVICE_HOST", "localhost"))
	c.ReviewServicePort = cast.ToString(getOrReturnDefault("REVIEW_SERVICE_PORT", "1111"))

	c.KafkaHost = cast.ToString(getOrReturnDefault("KAFKA_HOST", "kafka"))
	c.KafkaPort = cast.ToInt(getOrReturnDefault(`KAFKA_PORT`, 29092))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOGLEVEL", "debug"))
	c.RPCPort = cast.ToString(getOrReturnDefault("RPC_PORT", ":2222"))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
