package kafka

import (
	"context"
	"fmt"
	"temp/config"
	"temp/pkg/logger"
	"temp/pkg/messagebroker"
	"time"

	"github.com/segmentio/kafka-go"
)


type KafkaProduce struct {
	kafkaWriter *kafka.Writer
	logger logger.Logger
}

func NewKafkaProducer(conf config.Config, log logger.Logger, topic string) messagebroker.Producer {
	connString := fmt.Sprintf(`%s:%d`, conf.KafkaHost, conf.KafkaPort)
	
	return &KafkaProduce{
		kafkaWriter: &kafka.Writer{
			Addr: kafka.TCP(connString),
			Topic: topic,
			BatchTimeout: time.Millisecond * 10,
		},
		logger: log,
	}
}

func (k *KafkaProduce) Start() error {
	return nil
}

func (k *KafkaProduce) Stop() error {
	err := k.kafkaWriter.Close()
	if err != nil {
		return err
	}
	return nil
}

func (k *KafkaProduce) Produce(key, body []byte, logBody string) error {
	message := kafka.Message {
		Key: key,
		Value: body,
	}

	if err := k.kafkaWriter.WriteMessages(context.Background(),message); err != nil {
		return err
	}

	k.logger.Info(`Message produce(key/body):` + string(key) + "/" + logBody)
	
	return nil
} 
