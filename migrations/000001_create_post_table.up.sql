CREATE TABLE IF NOT EXISTS post (
    id serial PRIMARY KEY,
    customer_id INT,
    name TEXT,
    description TEXT,
    about TEXT, 
    created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    deleted_at TIMESTAMPTZ
)