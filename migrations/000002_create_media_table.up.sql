CREATE TABLE IF NOT EXISTS media (
    post_id INT NOT NULL REFERENCES post(id),
    name TEXT,
    link TEXT,
    type TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    deleted_at TIMESTAMPTZ
)