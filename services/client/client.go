package client

import (
	"fmt"
	"temp/config"
	pc "temp/genproto/customer"
	pr "temp/genproto/review"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ReviewClientI interface {
	Review() pr.ReviewServiceClient
	Customer() pc.CustomerServiceClient
}

type ServiceManager struct {
	cfg             config.Config
	reviewService   pr.ReviewServiceClient
	customerService pc.CustomerServiceClient
}

func New(cfg config.Config) (*ServiceManager, error) {
	connReview, err := grpc.Dial(
		fmt.Sprintf("%s:%s", cfg.ReviewServiceHost, cfg.ReviewServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &ServiceManager{}, err
	}

	connCustomer, err := grpc.Dial(
		fmt.Sprintf("%s:%s", cfg.CustomServiceHost, cfg.CustomServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &ServiceManager{}, err
	}

	return &ServiceManager{
		cfg:             cfg,
		reviewService:   pr.NewReviewServiceClient(connReview),
		customerService: pc.NewCustomerServiceClient(connCustomer),
	}, nil
}

func (r *ServiceManager) Review() pr.ReviewServiceClient {
	return r.reviewService
}

func (r *ServiceManager) Customer() pc.CustomerServiceClient {
	return r.customerService
}
