package services

import (
	"context"
	"fmt"
	"temp/genproto/customer"
	pp "temp/genproto/post"
	"temp/genproto/review"

	"temp/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (r *PostService) CreatePost(ctx context.Context, req *pp.PostReq) (*pp.PostResp, error) {
	fmt.Println(`Enter		`)
	post, err := r.storage.Post().CreatePost(req)
	if err != nil {
		r.logger.Error("Error with Post", logger.Any("Error inserting", err))
		return &pp.PostResp{}, status.Error(codes.Internal, "something went wrong, please check POST info")
	}

	err = r.produceMessage(post)
	if err != nil {
		r.logger.Error(`Error with Kafka ProduceMessage`, logger.Any(`Error inserting`, err))
		return &pp.PostResp{}, status.Error(codes.Internal, `something went wrong, please check KAFKA info`)
	}

	fmt.Println(`Exit		`)
	return post, nil
}

func (r *PostService) UpdatePost(ctx context.Context, req *pp.PostResp) (*pp.PostResp, error) {
	posts, err := r.storage.Post().UpdatePost(req)
	if err != nil {
		r.logger.Error("Error with Post", logger.Any("Error Updating ALL", err))
		return &pp.PostResp{}, status.Error(codes.Internal, "something went wrong, please check POST info")
	}
	return posts, nil
}

func (r *PostService) DeletePost(ctx context.Context, req *pp.PostID) (*pp.Empty, error) {
	err := r.storage.Post().DeletePost(req)
	if err != nil {
		r.logger.Error("Error while Deleting post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't delete your post")
	}
	return &pp.Empty{}, nil
}

func (r *PostService) GetPosts(ctx context.Context, req *pp.PostID) (*pp.PostResp, error) {
	posts, err := r.storage.Post().GetPosts(req)
	if err != nil {
		r.logger.Error("Error with Post", logger.Any("Error getting ALL", err))
		return &pp.PostResp{}, status.Error(codes.Internal, "something went wrong, please check POST info")
	}
	return posts, nil
}

func (r *PostService) GetPostWithAll(ctx context.Context, req *pp.PostID) (*pp.PostRespAllinfo, error) {
	posts, err := r.storage.Post().GetPosts(req)
	if err != nil {
		r.logger.Error("Error with post and media", logger.Any("Error with Service", err))
		return &pp.PostRespAllinfo{}, status.Error(codes.Internal, "please check your info")
	}

	responce := &pp.PostRespAllinfo{
		Id:          posts.Id,
		Name:        posts.Name,
		Description: posts.Description,
		About:       posts.About,
		Media:       posts.Media,
	}
	fmt.Println(`GetReview 				`)
	reviews, err := r.client.Review().GetReviewByPostID(ctx, &review.ReviewID{ReviewID: responce.Id})
	if err != nil {
		r.logger.Error("Error with review", logger.Any("Error with Service", err))
		return nil, status.Error(codes.Internal, "please check your info")
	}

	for _, review := range reviews.Reviews {
		responce.Review = append(responce.Review, &pp.ReviewResp{
			Id:          review.Id,
			CustomerId:  review.CustomerId,
			PostId:      review.PostId,
			Rating:      review.Rating,
			Description: review.Description,
		})
	}
	fmt.Println(`GetCustomer			`)

	customer, err := r.client.Customer().GetCustomers(ctx, &customer.CustomerID{Id: posts.CustomerId})
	if err != nil {
		fmt.Println(err, "THIS ERRORRRRR")
		r.logger.Error("Error with Customer", logger.Any("Error with Service", err))
		return nil, status.Error(codes.Internal, "please check your info")
	}
	ls_customer := &pp.CustomerResp{
		Id:          customer.Id,
		FirstName:   customer.FirstName,
		LastName:    customer.LastName,
		Wikipedia:   customer.Wikipedia,
		Email:       customer.Email,
		PhoneNumber: customer.PhoneNumber,
	}
	for _, cus := range customer.Address {
		ls_customer.Address = append(ls_customer.Address, &pp.Address{
			CustomerId: cus.CustomerId,
			District:   cus.District,
			Street:     cus.Street,
			HomeNumber: cus.HomeNumber,
		})
	}
	responce.Customer = ls_customer

	fmt.Println("Success					`")
	return responce, nil
}

func (r *PostService) GetPostWithReview(ctx context.Context, req *pp.PostID) (*pp.PostswithReview, error) {
	posts, err := r.storage.Post().GetPostWithReview(req)
	if err != nil {
		r.logger.Error("Error with posts and media", logger.Any("Error with Service", err))
		return &pp.PostswithReview{}, status.Error(codes.Internal, "please check your info")
	}
	responce := &pp.PostswithReview{}

	for _, post := range posts {
		temp := &pp.PostRespAllinfo{
			Id:          post.Id,
			Name:        post.Name,
			Description: post.Description,
			About:       post.About,
			Media:       post.Media,
		}
		reviews, err := r.client.Review().GetReviewByPostID(ctx, &review.ReviewID{ReviewID: temp.Id})
		if err != nil {
			r.logger.Error("Error with Review", logger.Any("Error With Service", err))
			reviews = &review.ReviewList{}
		}
		for _, rew := range reviews.Reviews {
			temp.Review = append(temp.Review, &pp.ReviewResp{
				Id:          rew.Id,
				CustomerId:  rew.CustomerId,
				PostId:      rew.PostId,
				Rating:      rew.Rating,
				Description: rew.Description,
			})
		}
		responce.Posts = append(responce.Posts, temp)
	}
	return responce, nil
}

func (p *PostService) GetPostBySearch(ctx context.Context, req *pp.GetPostSearchReq) (*pp.ListPostResp, error) {
	posts, err := p.storage.Post().GetPostBySearch(req)
	if err != nil {
		p.logger.Error(`Error with posts searching`, logger.Any(`Error with Post`, err))
		return &pp.ListPostResp{}, status.Error(codes.Internal, `please check your info`)
	}
	responce := &pp.ListPostResp{}
	for _, post := range posts.Posts {
		temp := &pp.PostResp{
			Id:          post.Id,
			CustomerId:  post.CustomerId,
			Name:        post.Name,
			Description: post.Description,
			About:       post.About,
		}
		responce.Posts = append(responce.Posts, temp)
	}

	return responce, nil
}
