package services

import (
	"fmt"
	"temp/genproto/post"
	"temp/pkg/logger"
	"temp/pkg/messagebroker"
	client "temp/services/client"
	"temp/storage"

	"github.com/jmoiron/sqlx"
)

type PostService struct{
	storage storage.IStorage
	logger logger.Logger
	client client.ReviewClientI
	producer map[string]messagebroker.Producer

}

func NewPostService(db *sqlx.DB, log logger.Logger, client client.ReviewClientI, producer map[string]messagebroker.Producer) *PostService {
	return &PostService{
		storage: storage.NewStoragePg(db),
		logger: log,
		client: client,
		producer: producer,
	}
}

func (p *PostService) produceMessage(raw *post.PostResp) error {
	data, err := raw.Marshal()
	if err != nil {
		return err
	}
	logPost := raw.String()
	err = p.producer["post"].Produce([]byte("post"), data, logPost)
	fmt.Println(err)
	if err != nil {
		return err
	}
	return nil
}