package repo

import pp "temp/genproto/post"

type PostStorageI interface{
	
	CreatePost(*pp.PostReq) (*pp.PostResp, error)
	UpdatePost(*pp.PostResp) (*pp.PostResp, error)
	GetPosts(*pp.PostID) (*pp.PostResp, error)
	DeletePost(*pp.PostID) error
	GetPostWithReview(*pp.PostID) ([]*pp.PostResp, error)
	GetPostBySearch(*pp.GetPostSearchReq) (*pp.ListPostResp, error)
	
	CreateMedia(*pp.Media) error
	DeletedMedia(*pp.PostID) error
	
	
	// GetAllPosts(*pp.PostID) ([]*pp.PostResp, error)
	// GetMediaByPostID(postID *pp.Media) (*pp.Media, error)
}