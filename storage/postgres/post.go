package postgres

import (
	"fmt"
	pp "temp/genproto/post"
)


func(p *postRepo) CreatePost(req *pp.PostReq) (*pp.PostResp, error) {
	responce := &pp.PostResp{}
	err := p.Db.QueryRow(`INSERT INTO post(customer_id, name, description, about) VALUES($1, $2, $3, $4)
	returning id, customer_id, name, description, about`,req.CustomerId, req.Name, req.Description, req.About).Scan(
		&responce.Id, &responce.CustomerId, &req.Name, &req.Description, &req.About)
	if err != nil {
		return  nil, err
	}
	// tr, err := p.Db.Begin()
	// if err != nil{
	// 	return nil, err
	// }
	for _, insert := range req.Media{
		insert.PostId = responce.Id
		
		err = p.CreateMedia(insert)
		if err != nil{
			// tr.Rollback()
			return &pp.PostResp{}, err
		}
		responce.Media = append(responce.Media, insert)
	}
	// tr.Commit()
	return responce, err
}

func(p *postRepo) UpdatePost(req *pp.PostResp) (*pp.PostResp, error) {
	err := p.Db.QueryRow(`UPDATE post SET customer_id=$1, name=$2, description=$3, about=$4, updated_at=NOW() 
	where id=$5 and deleted_at IS NULL`,req.CustomerId, req.Name, req.Description, req.About, req.Id).Err()
	if err != nil{
		return &pp.PostResp{}, err
	}
	err = p.DeletedMedia(&pp.PostID{PostId: req.Id})
	if err != nil{
		return &pp.PostResp{}, err
	}

	for _, media := range req.Media{
		err := p.CreateMedia(media)
		if err != nil{
			return nil, err
		}	
	}
	return req, nil
}

func(p *postRepo) GetPosts(req *pp.PostID) (*pp.PostResp, error) {
	posts := &pp.PostResp{}
	err := p.Db.QueryRow(`SELECT id, customer_id, name, description, about FROM post where id=$1 and 
	deleted_at IS NULL`,req.PostId).Scan(&posts.Id, &posts.CustomerId, &posts.Name, &posts.Description, &posts.About)
	if err != nil{
		return &pp.PostResp{}, err
	}
	posts.Media, err = p.GetMediaByPostID(pp.PostID{PostId: req.PostId})
	if err != nil{
		return &pp.PostResp{}, err
	}
	return posts, nil	
}

func(p *postRepo) DeletePost(req *pp.PostID) error {
	err := p.Db.QueryRow(`update post SET deleted_at=NOW() where id=$1 and deleted_at is null`,req.PostId).Err()
	if err != nil{
		return err
	}
	err = p.DeletedMedia(req)
	if err != nil{
		fmt.Println("Whatsuuuuuuuuuuuuuuup")
		return err
	}
	return err
}

func(p *postRepo) GetPostWithReview(req *pp.PostID) ([]*pp.PostResp, error) {
	rows, err := p.Db.Query(`select id, customer_id, name, description, about from post where customer_id=$1 and deleted_at is null`,req.PostId)
	if err != nil{
		return nil, err
	}
	defer rows.Close()
	posts := []*pp.PostResp{}
	
	for rows.Next(){
		temp := &pp.PostResp{}
		err := rows.Scan(&temp.Id, &temp.CustomerId, &temp.Name, &temp.Description, &temp.About)
		if err != nil{
			return nil, err
		}
		temp.Media, err = p.GetMediaByPostID(pp.PostID{PostId: temp.Id})
		if err != nil{
			temp.Media = nil
		}
		posts = append(posts, temp)	
		}
	return posts, nil
}

func(p *postRepo) GetPostBySearch(req *pp.GetPostSearchReq) (*pp.ListPostResp, error) {
	offset := (req.Page - 1) * req.Limit
	search := fmt.Sprintf("%s LIKE $1", req.Search.Field)
	order := fmt.Sprintf("ORDER BY %s %s", req.Orders.Field, req.Orders.Value)
	
	query := `SELECT id, customer_id, name, description, about FROM post where deleted_at IS NULL` + " and " + search + " " + order + " "
	rows, err := p.Db.Query(query + "LIMIT $2 OFFSET $3", "%" + req.Search.Value + "%", req.Limit, offset)
	if err != nil{
		return &pp.ListPostResp{}, err
	}
	defer rows.Close()
	
	responce := &pp.ListPostResp{}
	for rows.Next(){
		temp := &pp.PostResp{}
		err = rows.Scan(&temp.Id, &temp.CustomerId, &temp.Name, &temp.Description, &temp.About)
		if err != nil{
			return &pp.ListPostResp{}, err
		}
		responce.Posts = append(responce.Posts, temp)
	}

return responce, nil
}

