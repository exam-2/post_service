package postgres

import pp "temp/genproto/post"


func (m *postRepo) CreateMedia(req *pp.Media) error {
	return m.Db.QueryRow(`INSERT INTO media(post_id, name, link, type) VALUES($1, $2, $3, $4)`,req.PostId, req.Name, req.Link, req.Type).Err()
}

func (m *postRepo) DeletedMedia(postId *pp.PostID) error {
	return m.Db.QueryRow(`UPDATE media SET updated_at=NOW() WHERE post_id=$1 and deleted_at IS NULL`,postId.PostId).Err()
}

// func (m *postRepo) GetMediaByPostID(req *pp.PostID) (*pp.PostResp, error) {
// 	post := &pp.PostResp{}
// 	rows, err := m.Db.Query(`select id, customer_id, name, description, about from post where id=$1`,req.PostId)
// 	if err != nil{
// 		return &pp.PostResp{}, err
// 	}
// 	defer rows.Close()
// 	for rows.Next(){
// 		media := &pp.Media{}
// 		err := rows.Scan(&media.PostId, &media.Name, &media.Link, &media.Type)
// 		if err != nil{
// 			return &pp.PostResp{}, err
// 		}
// 	post.Media = append(post.Media, media)
// 	}
// 	return post, nil
// }

func (m *postRepo) GetMediaByPostID(req pp.PostID) ([]*pp.Media, error) {
	responce := []*pp.Media{}
	rows, err := m.Db.Query(`SELECT post_id, name, link, type FROM media WHERE post_id = $1 and deleted_at IS NULL`,req.PostId)
	if err != nil{
		return nil, err
	}
	defer rows.Close()

	for rows.Next(){
		temp := &pp.Media{}
		err = rows.Scan(&temp.PostId, &temp.Name, &temp.Link, &temp.Type)
		if err != nil{
			return nil, err
		}
		responce = append(responce, temp)
	}
return responce, nil
}

