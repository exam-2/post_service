package postgres

import "github.com/jmoiron/sqlx"

type postRepo struct{
	Db	*sqlx.DB
}

func NewPostRepo(db *sqlx.DB) *postRepo{
	return &postRepo{Db: db}
}
